<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'landingsp1' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g<ke_qRS0zV35xy2H|veyX*}|X^zzGmb0xvnij0f/xINR4q%Pw<mp%uII~51;PK5' );
define( 'SECURE_AUTH_KEY',  '#Kqe*r; s#E+8mYn%r<YQCj)@f>fK@AGe0!mES5H}|&*9.mHQWs{vyG!C&V_(,ZS' );
define( 'LOGGED_IN_KEY',    'rTM[%znU2%ohP{*{jZVue>]>Ud;tu2+#0$n$dk>/yS/+o|Mg&S|9}[^K5Z4*9!q+' );
define( 'NONCE_KEY',        'g6H:#hsg^:+vg5)Vzsy~_]%%?-frve^uM 9V3Q]SBjcMaTZ7#!$dZbe4+sZO/xE)' );
define( 'AUTH_SALT',        'Sw6Wn#0vJ/l#7uQkkE!gv7yG}];xSIX L*SUL]V)qr;QI`omGS7dn`%C?#QWntJ<' );
define( 'SECURE_AUTH_SALT', 'w1#S-/hx#z*EM<Ox5{lMBw*l66toN{`-Ai)C7RP=N*a&2$h,iKo*$UCe=jror SG' );
define( 'LOGGED_IN_SALT',   'qmL:x66oBqQYR$#42Yta#an-~5 K~CIIdNO_ZegQ1&e!>!6HZg/kN%{fZymuFu>c' );
define( 'NONCE_SALT',       'K,5=u;ulbPD._Aq c_jid8F2X5X7y Vf Bfn7@UtE1_*#f ;h8Jv4]543Yx{SIq ' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'sp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
