<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sanpham
 */

get_header();
$home_options = get_field('home', 'options');
?>

<div class="page-main">
	<?php if(isset($home_options['group_post_1']) && !empty($home_options['group_post_1']['post_1'])) : 
		$post = $home_options['group_post_1']['post_1']; 
	?>
	<section class="mb-3 mb-md-0">
		<a class="section-banner section section-background text-white" style="background-image:url(<?= get_the_post_thumbnail_url($post->ID) ?>" href="<?= get_permalink($post->ID) ?>">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<h2 class="section-title mb-0"><?= get_cat_field($post->ID, 'name') ?></h2>
						<p class="section-lead mb-0"><?= $post->post_title ?></p>
					</div>
				</div>
			</div>
			
			<?php if(get_post_format($post->ID) == 'video') : ?>
			<i class="section-icon-play icont-play-circle"></i>
			<?php endif; ?>
		</a>
	</section>
	<?php endif; ?>
	<section>
		<div class="row no-gutters mb-md">
			<?php if(isset($home_options['group_post_1']) && !empty($home_options['group_post_1']['post_2'])) : 
				$post = $home_options['group_post_1']['post_2']; 
			?>
			<div class="col-md-8 d-flex">
				<div class="card card-1">
					<div class="row no-gutters flex-column-reverse flex-md-row">
						<div class="col-md-3">
							<div class="card-body">
								<p class="card-meta small text-muted mb-1"><a href="<?=  get_category_link(get_cat_field($post->ID, 'term_id')) ?>"><?= get_cat_field($post->ID, 'name') ?></a></p>
								<h2 class="card-title text-uppercase mb-0">
									<a href="<?= get_permalink($post->ID) ?>" title="<?= $post->post_title ?>"><?= $post->post_title ?></a>
								</h2>
							</div>
						</div>
						<div class="col-md-9 d-flex">
							<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
								<div class="thumbnail-inner">
									<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
									<?php if(get_post_format($post->ID) == 'video') : ?>
										<div class="thumbnail-action">
											<i class="thumbnail-action-icon icont-play-circle"></i>
										</div>
									<?php endif; ?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php if(isset($home_options['group_post_1']) && !empty($home_options['group_post_1']['post_3'])) : 
				$post = $home_options['group_post_1']['post_3']; 
			?>
			<div class="col-md-4 d-flex">
				<div class="card card-1">
					<div class="row no-gutters flex-column-reverse flex-md-row">
						<div class="col-md-6">
							<div class="card-body">
								<p class="card-meta small text-muted mb-1"><a href="<?=  get_category_link(get_cat_field($post->ID, 'term_id')) ?>"><?= get_cat_field($post->ID, 'name') ?></a></p>
								<h2 class="card-title text-uppercase mb-0">
									<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
								</h2>
							</div>
						</div>
						<div class="col-md-6 d-flex">
							<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
								<div class="thumbnail-inner">
									<img class="thumbnail-img thumbnail-img-size-1" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
									<?php if(get_post_format($post->ID) == 'video') : ?>
										<div class="thumbnail-action">
											<i class="thumbnail-action-icon icont-play-circle"></i>
										</div>
									<?php endif; ?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<div class="card card-1 bg-gray">
			<?php if(isset($home_options['group_post_2']) && !empty($home_options['group_post_2']['post_1'])) : 
				$post = $home_options['group_post_2']['post_1']; 
			?>
			<div class="row no-gutters flex-column-reverse flex-md-row">
				<div class="col-md-3">
					<div class="card-body">
						<p class="card-meta small text-muted mb-1"><a href="<?= get_category_link(get_cat_field($post->ID, 'term_id')) ?>"><?= get_cat_field($post->ID, 'name') ?></a></p>
						<h2 class="card-title card-title-2 text-uppercase mb-0">
							<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
						</h2>
					</div>
				</div>
				<div class="col-md-9 d-flex">
					<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
						<div class="thumbnail-inner">
							<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID) ?>" alt="<?= $post->post_title ?>" />
							<?php if(get_post_format($post->ID) == 'video') : ?>
								<div class="thumbnail-action">
									<i class="thumbnail-action-icon icont-play-circle"></i>
								</div>
							<?php endif; ?>
						</div>
					</a>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<div class="row no-gutters mb-md">
			<?php if(isset($home_options['group_post_2']) && !empty($home_options['group_post_2']['post_2'])) : 
				$post = $home_options['group_post_2']['post_2']; 
			?>
			<div class="col-md-6 d-flex">
				<div class="card card-1">
					<div class="row no-gutters flex-column-reverse flex-md-row">
						<div class="col-md-4">
							<div class="card-body">
								<p class="card-meta small text-muted mb-1"><a href="<?= get_category_link(get_cat_field($post->ID, 'term_id')) ?>"><?= get_cat_field($post->ID, 'name') ?></a></p>
								<h2 class="card-title text-uppercase mb-0">
									<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
								</h2>
							</div>
						</div>
						<div class="col-md-8 d-flex">
							<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
								<div class="thumbnail-inner">
									<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
									<?php if(get_post_format($post->ID) == 'video') : ?>
										<div class="thumbnail-action">
											<i class="thumbnail-action-icon icont-play-circle"></i>
										</div>
									<?php endif; ?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php if(isset($home_options['group_post_2']) && !empty($home_options['group_post_2']['post_3'])) : 
				$post = $home_options['group_post_2']['post_3']; 
			?>
			<div class="col-md-6 d-flex">
				<div class="card card-1">
					<div class="row no-gutters flex-column-reverse flex-md-row">
						<div class="col-md-4">
							<div class="card-body">
								<p class="card-meta small text-muted mb-1"><a href="<?= get_category_link(get_cat_field($post->ID, 'term_id')) ?>"><?= get_cat_field($post->ID, 'name') ?></a></p>
								<h2 class="card-title text-uppercase mb-0">
									<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
								</h2>
							</div>
						</div>
						<div class="col-md-8 d-flex">
							<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
								<div class="thumbnail-inner">
									<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
									<?php if(get_post_format($post->ID) == 'video') : ?>
										<div class="thumbnail-action">
											<i class="thumbnail-action-icon icont-play-circle"></i>
										</div>
									<?php endif; ?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>

		<!--- BLOCK 3 -->
		<?php if(isset($home_options['group_post_3'])) : ?>
		<div class="row no-gutters">
			<?php for ($i=1; $i <= 6; $i++) { 
			if(!empty($home_options['group_post_3']['post_' . $i])) {
				$post = $home_options['group_post_3']['post_' . $i]; 
			?>
			<div class="col-md-4">
				<div class="card card-2">
					<div class="card-img">
						<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
						<?php if(get_post_format($post->ID) == 'video') : ?>
							<div class="thumbnail-action">
								<i class="thumbnail-action-icon icont-play-circle"></i>
							</div>
						<?php endif; ?>
					</div>
					<div class="card-img-overlay">
						<p class="card-meta text-uppercase mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
						<h2 class="card-title text-uppercase mb-0">
							<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
						</h2>
					</div>
				</div>
			</div>
			<?php }} ?>
		</div>
		<?php endif; ?>
		<div class="row no-gutters mb-md">
			<?php if(isset($home_options['group_post_3']) && !empty($home_options['group_post_3']['post_7'])) : 
						$post = $home_options['group_post_3']['post_7'];
			?>
			<div class="col-md-8 d-flex">
				<div class="card card-1">
					<div class="row no-gutters flex-column-reverse flex-md-row">
						<div class="col-md-3">
							<div class="card-body">
								<p class="card-meta small text-muted mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
								<h2 class="card-title text-uppercase mb-0">
									<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
								</h2>
							</div>
						</div>
						<div class="col-md-9 d-flex">
							<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
								<div class="thumbnail-inner">
									<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
									<?php if(get_post_format($post->ID) == 'video') : ?>
										<div class="thumbnail-action">
											<i class="thumbnail-action-icon icont-play-circle"></i>
										</div>
									<?php endif; ?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
			<?php if(isset($home_options['group_post_3']) && !empty($home_options['group_post_3']['post_7'])) : 
						$post = $home_options['group_post_3']['post_7'];
			?>
			<div class="col-md-4 d-flex">
				<div class="card card-1">
					<div class="row no-gutters flex-column-reverse flex-md-row">
						<div class="col-md-6">
							<div class="card-body">
							<p class="card-meta small text-muted mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
								<h2 class="card-title text-uppercase mb-0">
									<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
								</h2>
							</div>
						</div>
						<div class="col-md-6 d-flex">
							<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
								<div class="thumbnail-inner">
									<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
									<?php if(get_post_format($post->ID) == 'video') : ?>
										<div class="thumbnail-action">
											<i class="thumbnail-action-icon icont-play-circle"></i>
										</div>
									<?php endif; ?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div> <!--- END BLOCK 3 -->
		
		<!--- BLOCK 4 -->
		<?php if(isset($home_options['group_post_4'])) : ?>
		<div class="row no-gutters">
			<?php for ($i=1; $i <= 6; $i++) { 
			if(!empty($home_options['group_post_4']['post_' . $i])) {
				$post = $home_options['group_post_4']['post_' . $i]; 
			?>
			<div class="col-md-4">
				<div class="card card-2">
					<div class="card-img">
						<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
						<?php if(get_post_format($post->ID) == 'video') : ?>
							<div class="thumbnail-action">
								<i class="thumbnail-action-icon icont-play-circle"></i>
							</div>
						<?php endif; ?>
					</div>
					<div class="card-img-overlay">
						<p class="card-meta text-uppercase mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
						<h2 class="card-title text-uppercase mb-0">
							<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
						</h2>
					</div>
				</div>
			</div>
			<?php }} ?>
		</div>
		<?php endif; ?>
		
		<?php if(isset($home_options['group_post_4']) && !empty($home_options['group_post_4']['post_7'])) : 
			$post = $home_options['group_post_4']['post_7']; 
		?>
		<div class="card card-1 bg-gray mb-md">
			<div class="row no-gutters flex-column-reverse flex-md-row">
				<div class="col-md-3">
					<div class="card-body">
						<p class="card-meta small text-muted mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
						<h2 class="card-title card-title-2 text-uppercase mb-0">
						<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
						</h2>
					</div>
				</div>
				<div class="col-md-9 d-flex">
					<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
						<div class="thumbnail-inner">
							<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
							<?php if(get_post_format($post->ID) == 'video') : ?>
								<div class="thumbnail-action">
									<i class="thumbnail-action-icon icont-play-circle"></i>
								</div>
							<?php endif; ?>
						</div>
					</a>
				</div>
			</div>
		</div>
		<?php endif; ?> <!--- END BLOCK 4 -->

		<!--- BLOCK 5 -->
		<?php if(isset($home_options['group_post_5'])) : ?>
		<div class="row no-gutters">
			<?php for ($i=1; $i <= 6; $i++) { 
			if(isset($home_options['group_post_5']['post_' . $i])) {
				$post = $home_options['group_post_5']['post_' . $i]; 
			?>
			<div class="col-md-4">
				<div class="card card-2">
					<div class="card-img">
						<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
						<?php if(get_post_format($post->ID) == 'video') : ?>
							<div class="thumbnail-action">
								<i class="thumbnail-action-icon icont-play-circle"></i>
							</div>
						<?php endif; ?>
					</div>
					<div class="card-img-overlay">
						<p class="card-meta text-uppercase mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
						<h2 class="card-title text-uppercase mb-0">
							<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
						</h2>
					</div>
				</div>
			</div>
			<?php }} ?>
		</div>
		<?php endif; ?>
		<div class="row no-gutters mb-md">
			<?php if(isset($home_options['group_post_5']) && !empty($home_options['group_post_5']['post_7'])) : 
						$post = $home_options['group_post_5']['post_7'];
			?>
			<div class="col-md-8 d-flex">
				<div class="card card-1">
					<div class="row no-gutters flex-column-reverse flex-md-row">
						<div class="col-md-3">
							<div class="card-body">
								<p class="card-meta small text-muted mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
								<h2 class="card-title text-uppercase mb-0">
									<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
								</h2>
							</div>
						</div>
						<div class="col-md-9 d-flex">
							<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
								<div class="thumbnail-inner">
									<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
									<?php if(get_post_format($post->ID) == 'video') : ?>
										<div class="thumbnail-action">
											<i class="thumbnail-action-icon icont-play-circle"></i>
										</div>
									<?php endif; ?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
			<?php if(isset($home_options['group_post_5']) && !empty($home_options['group_post_5']['post_8'])) : 
						$post = $home_options['group_post_5']['post_8'];
			?>
			<div class="col-md-4 d-flex">
				<div class="card card-1">
					<div class="row no-gutters flex-column-reverse flex-md-row">
						<div class="col-md-6">
							<div class="card-body">
							<p class="card-meta small text-muted mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
								<h2 class="card-title text-uppercase mb-0">
									<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
								</h2>
							</div>
						</div>
						<div class="col-md-6 d-flex">
							<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
								<div class="thumbnail-inner">
									<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID) ?>" alt="" />
									<?php if(get_post_format($post->ID) == 'video') : ?>
										<div class="thumbnail-action">
											<i class="thumbnail-action-icon icont-play-circle"></i>
										</div>
									<?php endif; ?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div> <!--- END BLOCK 5 -->

		<!--- BLOCK 6 -->
		<?php if(isset($home_options['group_post_6']) && !empty($home_options['group_post_6']['post_1'])) : 
			$post = $home_options['group_post_6']['post_1'];
		?>
		<div class="card card-1 bg-gray">
			<div class="row no-gutters flex-column-reverse flex-md-row">
				<div class="col-md-3">
					<div class="card-body">
						<p class="card-meta small text-muted mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
						<h2 class="card-title card-title-2 text-uppercase mb-0">
							<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
						</h2>
					</div>
				</div>
				<div class="col-md-9 d-flex">
					<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
						<div class="thumbnail-inner">
							<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID) ?>" alt="" />
							<?php if(get_post_format($post->ID) == 'video') : ?>
								<div class="thumbnail-action">
									<i class="thumbnail-action-icon icont-play-circle"></i>
								</div>
							<?php endif; ?>
						</div>
					</a>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="row no-gutters">
			<?php if(isset($home_options['group_post_6']) && !empty($home_options['group_post_6']['post_2'])) : 
				$post = $home_options['group_post_6']['post_2'];
			?>
			<div class="col-md-6 d-flex">
				<div class="card card-1">
					<div class="row no-gutters flex-column-reverse flex-md-row">
						<div class="col-md-4">
							<div class="card-body">
								<p class="card-meta small text-muted mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
								<h2 class="card-title text-uppercase mb-0">
								<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
								</h2>
							</div>
						</div>
						<div class="col-md-8 d-flex">
							<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
								<div class="thumbnail-inner">
									<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
									<?php if(get_post_format($post->ID) == 'video') : ?>
										<div class="thumbnail-action">
											<i class="thumbnail-action-icon icont-play-circle"></i>
										</div>
									<?php endif; ?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
			<?php if(isset($home_options['group_post_6']) && !empty($home_options['group_post_6']['post_3'])) : 
				$post = $home_options['group_post_6']['post_3'];
			?>
			<div class="col-md-6 d-flex">
				<div class="card card-1">
					<div class="row no-gutters flex-column-reverse flex-md-row">
						<div class="col-md-4">
							<div class="card-body">
								<p class="card-meta small text-muted mb-1"><?= get_cat_field($post->ID, 'name') ?></p>
								<h2 class="card-title text-uppercase mb-0">
								<a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
								</h2>
							</div>
						</div>
						<div class="col-md-8 d-flex">
							<a class="thumbnail" href="<?= get_permalink($post->ID) ?>">
								<div class="thumbnail-inner">
									<img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="" />
									<?php if(get_post_format($post->ID) == 'video') : ?>
										<div class="thumbnail-action">
											<i class="thumbnail-action-icon icont-play-circle"></i>
										</div>
									<?php endif; ?>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div> <!--- END BLOCK 6 -->
	</section>
</div>

<?php
get_footer();