<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sanpham
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?= get_template_directory_uri() ?>/assets/images/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="<?= get_template_directory_uri() ?>/assets/images/apple-touch-icon.png">

	<?php wp_head(); ?>
</head>
<body>
<?php wp_body_open(); ?>
<div class="page">
	<header class="header" id="header">
      <nav class="header-navbar navbar navbar-expand-lg navbar-flat" id="navbar">
        <div class="container-fluid">
          <a class="header-brand" href="<?= home_url() ?>">
            <img class="d-inline-block align-top" src="<?= get_template_directory_uri() ?>/assets/images/logo.svg" alt="Creative Studio">
          </a>
          <div class="collapse navbar-collapse" id="navbar-collapse">
            <button class="navbar-close d-lg-none" id="navbar-close">
              <i class="icont-close"></i>
            </button>
            <?php if ( has_nav_menu( 'top' ) ) : ?>
              <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
            <?php endif; ?>
          </div>
          <ul class="nav nav-actions mb-0">
            <li class="nav-item header-search dropdown" id="header-search">
              <a class="nav-link header-search-toggle" id="header-search-toogle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="nav-icon" data-feather="search"></i>
                <i class="nav-icon" data-feather="x"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right p-0">
                <form class="search search-style-1" id="search-form" action="/">
                  <div class="input-group">
                    <input id="search-form-s" class="form-control" type="text" name="s" placeholder="Search...">
                    <div class="input-group-append">
                      <button class="btn" type="button" title="Search" disabled>
                        <i data-feather="search"></i>
                      </button>
                    </div>
                  </div>
                  
                  <div id="search-results"></div>
                </form>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="back-to-top" href="#">
                <i class="nav-icon" data-feather="arrow-up"></i>
              </a>
            </li>
            <!-- <li class="nav-item dropdown">
              <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="nav-icon" data-feather="bell"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">
                  <div class="media align-items-center">
                    <div class="media-body text-uppercase text-truncate fs-base">Admin
                    </div>
                    <small class="text-muted ml-3">14:30</small>
                  </div>
                  <p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore</p>
                </a>
                <a class="dropdown-item" href="#">
                  <div class="media align-items-center">
                    <div class="media-body text-uppercase text-truncate fs-base">Admin
                    </div>
                    <small class="text-muted ml-3">14:30</small>
                  </div>
                  <p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore</p>
                </a>
                <a class="dropdown-item" href="#">
                  <div class="media align-items-center">
                    <div class="media-body text-uppercase text-truncate fs-base">Lorem ipsum dolor sit amet, consectetur adipisicing elit
                    </div>
                    <small class="text-muted ml-3">14:30</small>
                  </div>
                  <p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore</p>
                </a>
              </div>
            </li> -->
            <li class="nav-item dropdown dropdown-account">
              <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="rounded-circle" src="<?= get_template_directory_uri() ?>/assets/images/ex/avatars/avatar-2.jpg" alt="" width="30">
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?= wp_login_url(); ?>">Đăng nhập</a>
              </div>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" id="navbar-toggle" href="#">
                <i class="nav-icon" data-feather="more-vertical"></i>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
