<?php 
$post_gallery = get_field('post_gallery', get_the_ID());
$cats = get_the_category(get_the_ID());

if(is_array($cats) && count($cats) > 0){
    $category = array_pop($cats);
}
?>

<div class="col-xl-8 col-lg-7">
    <?php if( is_array($post_gallery) && count($post_gallery) > 0) : ?>
        <?php foreach($post_gallery as $obj) : ?>
            <?php if($obj['type'] == 'video') { ?>
                <video src="<?= $obj['url'] ?>" style="max-width: 100%;" controls></video>
            <?php }else{ ?>
                <img class="img-fluid w-100" src="<?= $obj['url'] ?>" alt="">
            <?php } ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<div class="col-xl-4 col-lg-5">
    <div class="post-body" data-plugin="sticky-kit">
        <?php if(isset($category)) : ?>
        <p class="post-meta small text-muted mb-1"><a href="<?= get_category_link($category->term_id) ?>" title="<?= $category->name ?>"><?= $category->name ?></a></p>
        <?php endif; ?>
        <h1 class="post-title mb-3 pb-3"><?php the_title(); ?></h1>
        <div class="post-content text-muted">
            <?php the_content(); ?>
        </div>
    </div>
</div>