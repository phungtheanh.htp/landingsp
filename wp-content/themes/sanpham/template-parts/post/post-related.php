<?php 
// $home_options = get_field('home', 'options');
$cats = get_the_category(get_the_ID());

if(count($cats) > 0){
    $category = array_pop($cats);

    $posts = get_posts([
        'category' => $category->term
    ]);
}
?>

<?php if(isset($posts) && count($posts) > 0) : ?>
<section class="section py-0 pl-3">
    <header class="section-header mb-3 pb-3">
        <h2 class="text-uppercase fw-7 mb-1">Cùng danh mục</h2>
        <?php if(isset($category)) : ?>
        <p class="small text-muted mb-0"><a href="<?= get_category_link($category->term_id); ?>"><?= $category->name ?></a></p>
        <?php endif; ?>
    </header>
    <div class="swiper swiper-position-buttons-1">
        <div class="swiper-container" data-plugin="swiper" data-options='{"spaceBetween":16,"slidesPerView":2,"breakpoints":{"768":{"slidesPerView":3},"992":{"slidesPerView":4}}}'>
            <div class="swiper-wrapper">
                <?php foreach($posts as $post) : ?>
                <div class="swiper-slide">
                    <a class="thumbnail mb-3 mb-md-4" href="<?= get_permalink($post->ID) ?>">
                        <div class="thumbnail-inner">
                            <img class="thumbnail-img" src="<?= get_the_post_thumbnail_url($post->ID, 'vertical-thumbnail') ?>" alt="<?= $post->post_title ?>" />
                        </div>
                    </a>
                    <h6 class="text-uppercase mb-1">
                        <a href="<?= get_permalink($post->ID) ?>"><?= $post->post_title ?></a>
                    </h6>
                    <p class="small text-muted mb-0"><?= $post->post_date ?></p>
                </div>
                <?php endforeach; ?>
            </div>
            <div class="swiper-buttons">
                <div class="swiper-button-prev">
                    <i class="fs-lg" data-feather="arrow-left"></i>
                </div>
                <div class="swiper-button-next">
                    <i class="fs-lg" data-feather="arrow-right"></i>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>