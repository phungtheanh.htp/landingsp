<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * Template Name: Dịch vụ
 * @package sanpham
 */

 get_header();
?>
<div class="page-main">
    <?php
    $categories = get_categories();
    if(count($categories) > 0){ 
        foreach($categories as $category){
            $posts = get_posts([
                'category'         => $category->term_id,
                'numberposts'      => 8,
                'orderby'          => 'date',
                'order'            => 'DESC',
            ]);

            if(count($posts) > 0) {
    ?>
    <section class="section py-0 pl-3 mt-6">
        <header class="section-header mb-3 pb-3">
            <h2 class="text-uppercase fw-7 mb-1"><a href="<?= get_category_link($category->term_id) ?>" title="<?= $category->name ?>"><?= $category->name ?></a></h2>
            <!-- <p class="small text-muted mb-0">Lorem ipsum</p> -->
        </header>
        <div class="swiper swiper-position-buttons-1">
            <div class="swiper-container" data-plugin="swiper" data-options='{"spaceBetween":16,"slidesPerView":2,"breakpoints":{"768":{"slidesPerView":3},"992":{"slidesPerView":4}}}'>
                <div class="swiper-wrapper">
                    <?php foreach($posts as $post) : ?>
                    <div class="swiper-slide">
                        <a class="thumbnail mb-3 mb-md-4" href="<?= get_permalink($post->ID) ?>" title="<?= $post->name ?>">
                            <div class="thumbnail-inner">
                                <img class="thumbnail-img" src="<?= get_the_post_thumbnail_url( $post->ID, 'vertibal-thumbnail' ); ?>" />
                                <?php if(get_post_format($post->ID) == 'video') : ?>
                                    <div class="thumbnail-action">
                                        <i class="thumbnail-action-icon icont-play-circle"></i>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </a>
                        <h6 class="text-uppercase mb-1">
                            <a href="chi-tiet.html"><?= $post->post_title ?></a>
                        </h6>
                        <p class="small text-muted mb-0"><?= $post->post_date ?></p>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="swiper-buttons">
                    <div class="swiper-button-prev">
                        <i class="fs-lg" data-feather="arrow-left"></i>
                    </div>
                    <div class="swiper-button-next">
                        <i class="fs-lg" data-feather="arrow-right"></i>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
            }
        }
    } 
    ?>
</div>

<?php get_footer(); ?>