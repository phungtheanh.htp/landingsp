<?php

/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage sanpham
 * @since sanpham 1.0
 * @version 1.0
 */
?>
<?php
wp_nav_menu(
	array(
		'theme_location' => 'top',
		'container'       => false,
		'menu_id'        => 'navbar-nav',
		'menu_class' => 'navbar-nav navbar-nav-hover navbar-nav-effect mx-auto',
	)
);
?>