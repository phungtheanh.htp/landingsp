<div class="col-md-3 mb-5 pr-3">
    <a class="thumbnail mb-3 mb-md-4" href="<?= esc_url( get_permalink() ) ?>">
        <div class="thumbnail-inner">
            <img class="thumbnail-img" src="<?= get_the_post_thumbnail_url( get_the_ID(), 'vertical-thumbnail') ?>" alt="" />
            <?php if(get_post_format($post->ID) == 'video') : ?>
                <div class="thumbnail-action">
                    <i class="thumbnail-action-icon icont-play-circle"></i>
                </div>
            <?php endif; ?>
        </div>
    </a>
    <h6 class="text-uppercase mb-1">
        <a href="<?= esc_url( get_permalink() ) ?>" rel="bookmark"><?= get_the_title() ?></a>
    </h6>
    <p class="small text-muted mb-0"><?= get_the_date() ?></p>
</div>