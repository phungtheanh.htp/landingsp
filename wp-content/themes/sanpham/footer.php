<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sanpham
 */

?>
	<footer class="footer text-center" id="footer">
      <div class="container-fluid">
        <label class="text-uppercase mb-3 mb-md-4" for="contact-email">Liên hệ với chúng tôi</label>
        <div class="search search-style-1 mx-auto mb-3 mb-md-4">
            <form id="subscribe-form" action="/" method="post" class="input-group input-group-lg">
            <input type="hidden" name="action" value="subscribe_email">
            <?php wp_nonce_field('subscribe-form') ?>
              <input class="form-control fs-base" id="contact-email" type="email" name="email" required="required" placeholder="User@gmail.com">
              <div class="input-group-append">
                <button id="subscribe-btn" class="btn btn-form" type="button">
                  <i data-feather="arrow-right"></i>
                </button>
              </div>
            </form>
        </div>
        <p class="small text-muted mb-0">Admicro CreativeStudio, Hotline <a class="text-muted" href="tel:19006789">1900 6789</a>
        </p>
      </div>
    </footer>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
