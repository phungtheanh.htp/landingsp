<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sanpham
 */

get_header();
?>
<div class="page-main">
	<section class="post">
		<div class="row no-gutters flex-column-reverse flex-lg-row">
			<?php
			// Start the Loop.
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/post/content', get_post_format() );

			endwhile; // End the loop.
			?>
		</div>
	</section>
	
	<?php get_template_part( 'template-parts/post/post-related'); ?>
	
</div>

<?php
get_footer();
