<?php

function get_cat_field($post_id, $field_name){
    $cats = get_the_category($post_id);

    if(count($cats) > 0){
        $category = array_pop($cats);
        
        return $category->$field_name;
    }

    return null;
}