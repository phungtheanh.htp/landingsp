<?php

/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package sanpham
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function sanpham_body_classes($classes)
{
	// Adds a class of hfeed to non-singular pages.
	if (!is_singular()) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if (!is_active_sidebar('sidebar-1')) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter('body_class', 'sanpham_body_classes');

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function sanpham_pingback_header()
{
	if (is_singular() && pings_open()) {
		printf('<link rel="pingback" href="%s">', esc_url(get_bloginfo('pingback_url')));
	}
}
add_action('wp_head', 'sanpham_pingback_header');


/**
 * Custom classes nav
 */

function custom_menu_list_classes($classes, $item, $args)
{
	if ($args->theme_location == 'top') {
		$classes[] = 'nav-item';

		if (in_array('menu-item-has-children', $item->classes)) {
			$classes[] = 'dropdown';
		}
	}
	// print_r($item);
	return $classes;
}
add_filter('nav_menu_css_class', 'custom_menu_list_classes', 1, 3);

function custom_menu_link_classes($classes, $item, $args)
{
	if ($args->theme_location == 'top') {
		$classes['class'] = 'nav-link';

		if (in_array('menu-item-has-children', $item->classes)) {
			$classes['class'] = 'nav-link dropdown-toggle';
		}

		// print_r($item);

		if ($item->menu_item_parent > 0) {
			if (in_array('menu-item-has-children', $item->classes)) {
				$classes['class'] = 'dropdown-item dropdown-toggle';
			} else {
				$classes['class'] = 'dropdown-item';
			}
		}
	}

	return $classes;
}
add_filter('nav_menu_link_attributes', 'custom_menu_link_classes', 1, 3);

function custom_submenu_css_classes($classes, $args)
{
	if ($args->theme_location == 'top') {
		$classes[] = 'dropdown-menu';
	}

	return $classes;
}

add_filter('nav_menu_submenu_css_class', 'custom_submenu_css_classes', 1, 3);

/**
 * Change login logo
 */
function my_login_logo()
{ ?>
	<style type="text/css">
		#login h1 a,
		.login h1 a {
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.svg);
			background-size: contain;
		}
	</style>
<?php }

add_action('login_enqueue_scripts', 'my_login_logo');

/**
 * Custon image crop size
 */
function remove_unused_image_size($sizes)
{
	unset($sizes['medium']);
	unset($sizes['large']);
	unset($sizes['medium_large']);
	unset($sizes['1536x1536']);
	unset($sizes['2048x2048']);

	return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'remove_unused_image_size');

if (function_exists('add_image_size')) {
	add_image_size('vertical-thumbnail', 460, 690, true);
}

/**
 * Search sugget handle
 */

function search_ajax_js()
{ ?>
	<script type="text/javascript">
		$(document).ready(function($) {
			var ajaxurl = '<?php echo admin_url('admin-ajax.php') ?>';

			$("#search-form-s").on("keyup", function(e) {
				$.ajax({
					type: "post",
					dataType: "json",
					url: '<?php echo admin_url('admin-ajax.php'); ?>',
					data: {
						action: 'search_form',
						s: e.target.value
					},
					beforeSend: function() {

					},
					success: function(res) {
						$('#search-results').html('');

						if (res.length > 0) {
							var html = `<div class="search-result dropdown-menu">
								<h6 class="dropdown-header">Gợi ý</h6>`;
							res.forEach(element => {
								html += `<a class="dropdown-item" href="${element.link}">${element.title}</a>`;
							});
							html += `</div>`;
							$('#search-results').html(html);
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log('The following error occured: ' + textStatus, errorThrown);
					}
				});

				return false;
			});
		});
	</script>
<?php
}

add_action('wp_footer', 'search_ajax_js');

add_action('wp_ajax_search_form', 'search_form_function');
add_action('wp_ajax_nopriv_search_form', 'search_form_function');
function search_form_function()
{
	if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'search_form') {

		if (!isset($_REQUEST['s'])) {
			echo 'error';
			exit;
		}

		$args = array(
			'posts_per_page'  => 5,
			'post_type' => ['post'],
			'post_status' => ['publish'],
			's'      => $_REQUEST['s'],
			'order' => 'DESC',
			'orderby' => 'date',
		);
		$the_query = new WP_Query($args);

		$json = [];
		while ($the_query->have_posts()) : $the_query->the_post();
			$json[] = [
				'title' => get_the_title(),
				'link'  => get_the_permalink(get_the_ID())
			];
		endwhile;

		echo json_encode($json);
		wp_reset_postdata();
	}

	wp_die();
}

function wpse_filter_child_cats($query)
{

	if ($query->is_category) {
		$queried_object = get_queried_object();
		$child_cats = (array) get_term_children($queried_object->term_id, 'category');

		if (!$query->is_admin)
			//exclude the posts in child categories
			$query->set('category__not_in', array_merge($child_cats));
	}

	return $query;
}
add_filter('pre_get_posts', 'wpse_filter_child_cats');