<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package sanpham
 */

get_header();
?>

	<div class="page-main">
		<section class="section py-0 pl-3 mt-6">
			<?php if ( have_posts() ) : ?>

			<header class="section-header mb-3 pb-3">
				<h1 class="text-uppercase fw-7 mb-1">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Từ khóa: %s', 'sanpham' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h1>
			</header><!-- .page-header -->
		</section>
		<div class="row no-gutters mb-md mb-md py-0 pl-3">
		<?php
		while ( have_posts() ) :
		the_post();

		get_template_part( 'template-parts/content', 'archive' );
		endwhile;

		wp_pagenavi();

		else :

		get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		</div>
	</div>

<?php
get_footer();
