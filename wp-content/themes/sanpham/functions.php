<?php
/**
 * sanpham functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sanpham
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'sanpham_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sanpham_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on sanpham, use a find and replace
		 * to change 'sanpham' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'sanpham', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'top' => esc_html__( 'Menu chính', 'sanpham' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'sanpham_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

		/*
		* Enable support for Post Formats.
		*
		* See: https://wordpress.org/support/article/post-formats/
		*/
		add_theme_support(
			'post-formats',
			array(
				'image',
				'video',
				'gallery'
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'sanpham_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sanpham_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sanpham_content_width', 640 );
}
add_action( 'after_setup_theme', 'sanpham_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sanpham_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'sanpham' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'sanpham' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'sanpham_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

// get_stylesheet_uri()

function footer_scripts(){
	echo '<script>feather.replace()</script>';
}

add_action( 'wp_footer', 'footer_scripts');

function sanpham_scripts() {
	wp_enqueue_style( 'sanpham-style', get_template_directory_uri() . '/assets/css/style.min.css', array(), _S_VERSION );
	wp_style_add_data( 'sanpham-style', 'rtl', 'replace' );
	wp_enqueue_style( 'sanpham-style-1', get_template_directory_uri() . '/style.css', array(), _S_VERSION );

	wp_enqueue_script( 'sanpham-js1', get_template_directory_uri() . '/assets/js/vendor/modernizr.custom.js', array(), _S_VERSION, false );
	wp_enqueue_script( 'sanpham-js2', get_template_directory_uri() . '/assets/js/vendor/feather.min.js', array(), _S_VERSION, false );
	wp_enqueue_script( 'sanpham-js3', get_template_directory_uri() . '/assets/js/vendor/jquery.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js4', get_template_directory_uri() . '/assets/js/vendor/bootstrap.bundle.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js5', get_template_directory_uri() . '/assets/js/vendor/swiper.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js6', get_template_directory_uri() . '/assets/js/vendor/jquery.sticky-kit.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js7', get_template_directory_uri() . '/assets/js/vendor/flatpickr.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js8', get_template_directory_uri() . '/assets/js/vendor/flatpickr.monthSelect.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js9', get_template_directory_uri() . '/assets/js/vendor/flatpickr-vn.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js10', get_template_directory_uri() . '/assets/js/vendor/float-labels.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js11', get_template_directory_uri() . '/assets/js/vendor/jquery.spinner.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js12', get_template_directory_uri() . '/assets/js/vendor/select2.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js13', get_template_directory_uri() . '/assets/js/vendor/select2.vi.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'sanpham-js14', get_template_directory_uri() . '/assets/js/main.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'sanpham_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * helpers.
 */
require get_template_directory() . '/inc/helpers.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * ACF cài đặt theme
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Cài đặt giao diện',
		'menu_title'	=> 'Cài đặt giao diện',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Header Settings',
	// 	'menu_title'	=> 'Header',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
	
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Theme Footer Settings',
	// 	'menu_title'	=> 'Footer',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
}

// Hide admin bar
// add_filter('show_admin_bar', '__return_false');

// hide update notifications
function remove_core_updates(){
	global $wp_version;
	return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}

add_filter('pre_site_transient_update_core','remove_core_updates'); //hide updates for WordPress itself
add_filter('pre_site_transient_update_plugins','remove_core_updates'); //hide updates for all plugins
add_filter('pre_site_transient_update_themes','remove_core_updates'); //hide updates for all themes