// Avoid `console` errors in browsers that lack a console.
(function () {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());


// Place any jQuery/helper plugins in here.
$(document).ready(function () {

});


// -------------------------
// back to top
// -------------------------

$(document).ready(function () {
  var $backToTop = $('#back-to-top');

  if ($backToTop.length) {
    $backToTop.on('click', function (e) {
      e.preventDefault();
      $('html, body').animate({
        scrollTop: 0
      }, 700);
    });
  }
});


// --------------------------
// navbar-toggle
// --------------------------

$(document).ready(function () {
  if ($(window).width() < 992) {
    var $navbarCollapse = $('#navbar-collapse'),
      $navbarToggle = $('#navbar-toggle'),
      $navbarDropdownToggle = $navbarCollapse.find('.dropdown-toggle'),
      $navbarOverlay = $('<div id="navbar-overlay" class="navbar-overlay"></div>');

    $navbarCollapse.after($navbarOverlay);

    $navbarOverlay.on('click', function () {
      closeNavbar();
    });

    $navbarDropdownToggle.on('click', function (e) {
      e.preventDefault();
      $(this).next().slideToggle(200);
      $(this).parent().toggleClass('show');
    });

    $navbarToggle.on('click', function (e) {
      e.preventDefault();
      // $(this).toggleClass('is-active');
      $navbarCollapse.toggleClass('show');
    });

    $('#navbar-close').on('click', function (e) {
      closeNavbar();
    });

    // listening the clicks outside of the nav-main to close it
    // $('body').on('click', function (e) {
    //   if (!$navbarCollapse.is(e.target) && $navbarCollapse.has(e.target).length === 0
    //     && !$navbarToggle.is(e.target) && $navbarToggle.has(e.target).length === 0
    //   ) {
    //     closeNavbar();
    //   }
    // });

    // header-search
    $('#header-search-toogle').on('click', function (e) {
      e.preventDefault();
      var $this = $(this);
      if ($this.attr('aria-expanded')) {
        setTimeout(function () {
          console.log($this.next().find('.form-control'));
          $this.next().find('.form-control').focus();
        }, 100);
      }
    });

    function closeNavbar() {
      $navbarToggle.trigger('click');
    }
  }

  // Prevent Bootstrap dropdown from closing on clicks
  $('.nav-actions .dropdown-menu').click(function(e) {
    e.stopPropagation();
  });
});


// -------------------------
// search
// -------------------------

$(document).ready(function () {
  var $headerSearch = $('#header-search'),
      $overlaySearch = $('<div id="overlay-search" class="overlay overlay-search"></div>')

  $headerSearch.on('show.bs.dropdown', function () {
    $headerSearch.append($overlaySearch);
    $overlaySearch.fadeIn(200);
    setTimeout(function () {
      $headerSearch.find('.form-control').focus();
    }, 100);
  });

  $headerSearch.on('hide.bs.dropdown', function () {
    $overlaySearch.fadeOut(200, function () {
      $(this).remove();
    });
  });
});


// --------------------------
// bootstrap
// --------------------------

$(document).ready(function(){
  // tooltip
  $('[data-toggle="tooltip"]').tooltip();

  // popover
  $('[data-toggle="popover"]').popover();
});


// --------------------------
// flatpickr
// --------------------------

(function ($) {
  $.fn.pluginFlatpickr = function (opts) {
    var defaults = {
      // altInput: true,
      // altFormat: "F j, Y",
      // altInputClass: 'form-control flatpickr-input-alt',
      dateFormat: "d-m-Y",
      wrap: true,
      locale: 'vn'
    };
    var options = $.extend({}, defaults, opts);

    $(this).flatpickr(options);
    return this;
  }
}(jQuery));

(function ($) {
  $.fn.pluginFlatpickrMonth = function (opts) {
    var defaults = {
      altInput: true,
      dateFormat: "d-m-Y",
      wrap: true,
      locale: 'vn',
      disableMobile: true,
      plugins: [
        new monthSelectPlugin({
          shorthand: true,
          dateFormat: "m/y",
          altFormat: "F, Y"
        })
      ]
    };
    var options = $.extend({}, defaults, opts);

    $(this).flatpickr(options);
    return this;
  }
}(jQuery));

// execute
$(document).ready(function () {
  $('[data-plugin="flatpickr"]').each(function () {
    $(this).pluginFlatpickr($(this).data('options'));
  });

  $('[data-plugin="flatpickr-month"]').each(function () {
    $(this).pluginFlatpickrMonth($(this).data('options'));
  });
});


// --------------------------
// Float Labels
// --------------------------

$(document).ready(function () {
  var floatlabels_1 = new FloatLabels('[data-plugin="float-labels-style-1"]', {
    style: 1
  });

  var floatlabels_2 = new FloatLabels('[data-plugin="float-labels-style-2"]', {
    style: 2
  });
});


// --------------------------
// sticky-kit
// --------------------------

(function ($) {
  $.fn.pluginStickyKit = function (opts) {
    var defaults = {};
    var options = $.extend({}, defaults, opts);

    $(this).stick_in_parent(options);
    return this;
  }
}(jQuery));

// execute
$(document).ready(function () {
  $('[data-plugin="sticky-kit"]').each(function () {
    $(this).pluginStickyKit($(this).data('options'));
  });
});


// --------------------------
// swiper
// --------------------------

(function ($) {
  $.fn.pluginSwiper = function (opts) {
    var defaults = {
      navigation: {
        nextEl: $(this).find('.swiper-button-next'),
        prevEl: $(this).find('.swiper-button-prev')
      },
      pagination: {
        el: $(this).find('.swiper-pagination'),
        clickable: true
      }
    };
    var options = $.extend({}, defaults, opts);

    new Swiper(this, options);
    return this;
  }
}(jQuery));

// execute
$(document).ready(function () {
  $('[data-plugin="swiper"]').each(function () {
    $(this).pluginSwiper($(this).data('options'));
  });
});


// --------------------------
// jquery-ui spinner
// --------------------------

(function ($) {
  $.fn.pluginSelect2 = function (opts) {
    var defaults = {
      minimumResultsForSearch: Infinity
    };
    var options = $.extend({}, defaults, opts);

    $(this).select2(options);
    return this;
  }
}(jQuery));

// execute
$(document).ready(function () {
  $('[data-plugin="select2"]').each(function () {
    $(this).pluginSelect2($(this).data('options'));
  });
});