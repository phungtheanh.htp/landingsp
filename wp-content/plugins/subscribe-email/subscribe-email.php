<?php

/**
 * Plugin Name: Subscribe email
 * Description: Quản lý đăng ký email
 * Version: 1.0
 * Author: phungtheanh.htp@gmail.com
 * Author URI: https://www.google.com
 * Text Domain: se_subscribe
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Version of the plugin
define('SUBSCRIBE_CURRENT_VERSION', '1.0' );
define('SEPATH', dirname(__FILE__) );

global $sp_subscribe;

if (is_admin()){
	require_once (SEPATH .'/subscribe-email-admin.php');
}else{
	require_once (SEPATH .'/subscribe-email-frontend.php');
}

add_action('wp_ajax_subscribe_email', 'subscribe_email_function');
add_action('wp_ajax_nopriv_subscribe_email', 'subscribe_email_function');
function subscribe_email_function()
{
    if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'subscribe_email'){
		global $wpdb;
		$table  = $wpdb->prefix . 'subscribe';

		if(!is_email($_REQUEST['email'])){
			echo 'error';
			exit;
		}

		$sql = "SELECT * FROM {$table} WHERE `email` = %s";
		$result = $wpdb->get_results( $wpdb->prepare($sql, $_REQUEST['email']), ARRAY_A);

		if(count($result) > 0){
			echo 'exist';
			exit;
		}else{
			date_default_timezone_set("Asia/Ho_Chi_Minh");

			$insert_query = $wpdb->insert($table, array(
				'email' 	=> $_REQUEST['email'],
				'active' 	=> 1,
				'date' 		=> date("Y-m-d"),
				'time' 		=> date("h:i:sa"),
				'ip' 		=> $_SERVER['REMOTE_ADDR']
			), array('%s', '%d', '%s', '%s', '%s', '%s'));
			
			if($insert_query){
				echo 'success';
			}else{
				echo 'error';
			}
			
			exit;
		}
	}

	wp_die();
}