<?php


// include custom jQuery
function shapeSpace_include_custom_jquery()
{
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, false);
}
add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');

// Add script to footer
function subscribe_ajax_js()
{ ?>
    <script type="text/javascript">
        $(document).ready(function($) {
            var ajaxurl = '<?php echo admin_url('admin-ajax.php') ?>';

            $("#subscribe-btn").on("click", function() {
                $.ajax({
                    type : "post",
                    dataType : "html",
                    url : '<?php echo admin_url('admin-ajax.php');?>',
                    data : $("#subscribe-form").serialize(),
                    beforeSend: function(){
                        
                    },
                    success: function(res) {
                        var ms = '';
                        var color = 'red';
                        
                        if(res == 'exist'){
                            ms = 'Email đã được đăng ký!'
                        }else if(res == 'error'){
                            ms = 'Đã có lỗi xảy ra vui lòng kiểm tra lại!'
                        }else if(res == 'success'){
                            color = 'green';
                            ms = 'Đăng ký thành công'
                        }

                        var style = `text-align: left; font-size: 13px; margin-top: 2px; position: absolute; color: ${color}`;

                        if($('#subscribe-ms').length > 0){
                            $('#subscribe-ms').text(ms);
                            $('#subscribe-ms').css('color', color);
                        }else{
                            $("#subscribe-form").after('<div id="subscribe-ms" style="' + style + '">' + ms + '</div>');
                        }
                    },
                    error: function( jqXHR, textStatus, errorThrown ){
                        console.log( 'The following error occured: ' + textStatus, errorThrown );
                    }
                });

                return false;
            });
        });
    </script>
<?php
}

add_action('wp_footer', 'subscribe_ajax_js');
