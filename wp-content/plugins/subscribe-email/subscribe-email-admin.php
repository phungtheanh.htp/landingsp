<?php
require_once SEPATH . '/classes/subscribe-list-table.php';

/**
 * Init plugin setting
 */
function admin_init()
{
    // register_setting('se-settings-group', 'se_option_name');
}
add_action('admin_init', 'admin_init');

/**
 * Add admin menu
 */
function add_admin_menu()
{
    add_menu_page('Quản lý đăng ký email', 'Đăng ký email', 'administrator', __FILE__, 'list_table_page', 'dashicons-email-alt', 5);
}
add_action('admin_menu', 'add_admin_menu');

/**
 * Display the list table page
 *
 * @return Void
 */
function list_table_page()
{
    $subscribeListTable = new Subscribe_List_Table();
    $subscribeListTable->prepare_items();
    ?>
        <div class="wrap">
            <div id="icon-users" class="icon32"></div>
            <h2>Email đăng ký</h2>
            <form id="subscribe-form" method="POST">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>" />
                <?php 
                    $subscribeListTable->search_box( __( 'Search', 'se_subscribe' ), 'email' ); 
                    $subscribeListTable->display(); 
                ?>
            </form>
        </div>
    <?php
}

add_action('init', 'do_output_buffer');
function do_output_buffer() {
        ob_start();
}

/**
 * Add a flash notice to {prefix}options table until a full page refresh is done
 *
 * @param string $notice our notice message
 * @param string $type This can be "info", "warning", "error" or "success", "warning" as default
 * @param boolean $dismissible set this to TRUE to add is-dismissible functionality to your notice
 * @return void
 */
 
function add_flash_notice( $notice = "", $type = "warning", $dismissible = true ) {
    // Here we return the notices saved on our option, if there are not notices, then an empty array is returned
    $notices = get_option( "my_flash_notices", array() );
 
    $dismissible_text = ( $dismissible ) ? "is-dismissible" : "";
 
    // We add our new notice.
    array_push( $notices, array( 
            "notice" => $notice, 
            "type" => $type, 
            "dismissible" => $dismissible_text
        ) );
 
    // Then we update the option with our notices array
    update_option("my_flash_notices", $notices );
}
 
/**
 * Function executed when the 'admin_notices' action is called, here we check if there are notices on
 * our database and display them, after that, we remove the option to prevent notices being displayed forever.
 * @return void
 */
 
function display_flash_notices() {
    $notices = get_option( "my_flash_notices", array() );
     
    // Iterate through our notices to be displayed and print them.
    foreach ( $notices as $notice ) {
        printf('<div class="notice notice-%1$s %2$s"><p>%3$s</p></div>',
            $notice['type'],
            $notice['dismissible'],
            $notice['notice']
        );
    }
 
    // Now we reset our options to prevent notices being displayed forever.
    if( ! empty( $notices ) ) {
        delete_option( "my_flash_notices", array() );
    }
}
 
// We add our display_flash_notices function to the admin_notices
add_action( 'admin_notices', 'display_flash_notices', 12 );