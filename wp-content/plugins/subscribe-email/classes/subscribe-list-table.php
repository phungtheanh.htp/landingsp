<?php

// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'Subscribe_Query' ) ) {
    require_once( SEPATH . '/classes/subscribe-email-query.php' );
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class Subscribe_List_Table extends WP_List_Table
{
    private $Se_Query;

    // public function __construct() {
	// 	global $status, $page;

	// 	parent::__construct(
	// 		array(
	// 			'singular' => 'subscriber',
	// 			'plural'   => 'subscribers',
	// 			'ajax'     => true,
	// 		)
	// 	);
    // }
    
    public function get_bulk_actions(){
        return array(
            'delete' => __( 'Xóa', 'se_subscribe' )
        );
    }

    public function process_bulk_action() {
        if ( in_array( $this->current_action(), array( 'delete' ), true ) ) {
			if ( ! isset( $_REQUEST['subscribe_ids'] ) ) {
				echo '<div id="message" class="error notice is-dismissible"><p><strong>' . esc_html__( 'Chưa chọn mục cần xóa.', 'sp_subscribe' ) . '</strong></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">'.esc_html__( 'Bỏ qua thông báo này.', 'sp_subscribe' ).'</span></button></div>';
				return;
			}
		}
        
		// security check!
        if ( isset( $_POST['_wpnonce'] ) && ! empty( $_POST['_wpnonce'] ) ) {

            $nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
            $action = 'bulk-' . $this->_args['plural'];

            if ( ! wp_verify_nonce( $nonce, $action ) )
                wp_die( 'Nope! Security check failed!' );

        }

        $action = $this->current_action();
        
        switch ( $action ) {
            case 'delete':
                $this->Se_Query->delete_subscribe($_REQUEST['subscribe_ids']);
                
                add_flash_notice( __("Xóa đăng ký thành công"), "success", true );

                wp_redirect( admin_url( 'admin.php?page=' . urlencode( wp_unslash( $_REQUEST['page'] ) ) ) );
                exit;
                break;
            default:
                // do nothing or something else
                return;
                break;
        }
	}

    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        global $s;

        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $s = isset( $_REQUEST['s'] ) ? wp_unslash( trim( $_REQUEST['s'] ) ) : '';

        $wild = '';
		if ( false !== strpos( $s, '*' ) ) {
			$wild = '*';
			$s    = trim( $s, '*' );
        }

        $per_page = 15;
        $paged = $this->get_pagenum();

        $args = array(
			'number'  => $per_page,
			'offset'  => ( $paged - 1 ) * $per_page,
			'search'  => $s,
			'id' => 0,
			'fields'  => 'all_with_meta',
        );
        
        $this->Se_Query = new Subscribe_Query( $args );

        $this->set_pagination_args( array(
            'total_items' => $this->Se_Query->get_total(),
            'per_page'    => $per_page
        ));

        $this->items = $this->Se_Query->get_results( $args );

        $this->process_bulk_action();
    }

    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        $columns = array(
            'cb'    => '<input type="checkbox" />',
            'email'       => 'Email',
            'ip'        => 'Ip',
            'date'      => 'Thời gian'
        );

        return $columns;
    }

    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array('email' => array('email', false));
    }

    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'email':
            case 'ip':
            case 'date':
                return $item[ $column_name ];
            default:
                return print_r( $item, true ) ;
        }
    }

    /**
	 * overide column checkbox
	 */
    public function column_cb( $item ) {
		return sprintf( '<input type="checkbox" name="%1$s[]" value="%2$s" />', 'subscribe_ids', $item['id'] );
	}

    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'email';
        $order = 'asc';

        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }

        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }


        $result = strcmp( $a[$orderby], $b[$orderby] );

        if($order === 'asc')
        {
            return $result;
        }

        return -$result;
    }
}
?>