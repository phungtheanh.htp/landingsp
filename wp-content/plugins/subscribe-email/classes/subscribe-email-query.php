<?php

/**
 * Subscribe_Query
 */

class Subscribe_Query
{
    /**
     * Kết quả trả về
     * @var array
     */
    private $results;

    /**
     * Tổng số email đăng ký
     * @var int
     */
    private $total_subscribes;

    // SQL clauses.
	public $query_fields;
	public $query_from;
	public $query_where;
	public $query_orderby;
	public $query_limit;

    public function __construct($query = null)
    {
        $this->prepare_query( $query );
    }

    public function prepare_query( $query = array() ){
        global $wpdb;

        if ( ! empty( $query ) ) {
			$this->query_limit = null;
		}

        $search = $query['search'];
        $limit  = $query['number'];
        $offset = $query['offset'];
        $table  = $wpdb->prefix . 'subscribe';

        $sql = "SELECT * FROM {$table} WHERE `email` LIKE %s ORDER BY date DESC LIMIT %d OFFSET %d";

        $this->results = $wpdb->get_results( $wpdb->prepare(
            $sql, 
            '%' . $wpdb->esc_like( $search ) . '%', 
            $limit,
            $offset
        ), ARRAY_A);

        $this->total_subscribes = $wpdb->get_var(
            $wpdb->prepare(
            "
                SELECT count(*)
                FROM {$table}
                WHERE `email` LIKE %s
            ", 
            '%' . $wpdb->esc_like( $search ) . '%'
        ));
    }

    public function delete_subscribe($ids = array()){
        global $wpdb;
        $table  = $wpdb->prefix . 'subscribe';

        $ids = implode(',', $ids);

        $wpdb->query(
            "
                DELETE
                FROM {$table}
                WHERE `id` IN ({$ids})
            ");
        
        return true;
    }

    public function get_results( $args ){
        return $this->results;
    }

    public function get_total(){
        return $this->total_subscribes;
    }
}
