<?php
/**
 * Awesomesauce class.
 *
 * @category   Class
 * @package    ElementorAwesomesauce
 * @subpackage WordPress
 * @author     Ben Marshall <me@benmarshall.me>
 * @copyright  2020 Ben Marshall
 * @license    https://opensource.org/licenses/GPL-3.0 GPL-3.0-only
 * @link       link(https://www.benmarshall.me/build-custom-elementor-widgets/,
 *             Build Custom Elementor Widgets)
 * @since      1.0.0
 * php version 7.3.9
 */

namespace ElementorAwesomesauce\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

// Security Note: Blocks direct access to the plugin PHP files.
defined( 'ABSPATH' ) || die();

/**
 * Awesomesauce widget class.
 *
 * @since 1.0.0
 */
class Awesomesauce extends Widget_Base {
	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	public function __construct( $data = array(), $args = null ) {
		parent::__construct( $data, $args );

		wp_register_style( 'awesomesauce', plugins_url( '/assets/css/awesomesauce.css', ELEMENTOR_AWESOMESAUCE ), array(), '1.0.0' );
	}

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'awesomesauce';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Awesomesauce', 'elementor-awesomesauce' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-pencil';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return array( 'general' );
	}
	
	/**
	 * Enqueue styles.
	 */
	public function get_style_depends() {
		return array( 'awesomesauce' );
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'section_content',
			array(
				'label' => __( 'Content', 'elementor-awesomesauce' ),
			)
		);

		$this->add_control(
			'title',
			array(
				'label'   => __( 'Title', 'elementor-awesomesauce' ),
				'type'    => Controls_Manager::TEXT,
				'default' => __( 'Title', 'elementor-awesomesauce' ),
			)
		);

		$this->add_control(
			'description',
			array(
				'label'   => __( 'Description', 'elementor-awesomesauce' ),
				'type'    => Controls_Manager::TEXTAREA,
				'default' => __( 'Description', 'elementor-awesomesauce' ),
			)
		);

		$this->add_control(
			'content',
			array(
				'label'   => __( 'Content', 'elementor-awesomesauce' ),
				'type'    => Controls_Manager::WYSIWYG,
				'default' => __( 'Content', 'elementor-awesomesauce' ),
			)
		);

		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_inline_editing_attributes( 'title', 'none' );
		$this->add_inline_editing_attributes( 'description', 'basic' );
		$this->add_inline_editing_attributes( 'content', 'advanced' );
		?>
		<!-- <h2 <?php echo $this->get_render_attribute_string( 'title' ); ?>><?php echo wp_kses( $settings['title'], array() ); ?></h2>
		<div <?php echo $this->get_render_attribute_string( 'description' ); ?>><?php echo wp_kses( $settings['description'], array() ); ?></div>
		<div <?php echo $this->get_render_attribute_string( 'content' ); ?>><?php echo wp_kses( $settings['content'], array() ); ?></div> -->
		<section class="mb-3 mb-md-0">
			<a class="section-banner section section-background text-white" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/ex/home/banner.jpg)" href="#">
			<div class="container-fluid">
				<div class="row">
				<div class="col-lg-4 col-md-6">
					<h2 class="section-title mb-0">Lorem ipsum</h2>
					<p class="section-lead mb-0">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy</p>
				</div>
				</div>
			</div>
			<i class="section-icon-play icont-play-circle"></i>
			</a>
		</section>
		<section>
        <div class="row no-gutters mb-md">
          <div class="col-md-8 d-flex">
            <div class="card card-1">
              <div class="row no-gutters flex-column-reverse flex-md-row">
                <div class="col-md-3">
                  <div class="card-body">
                    <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                    <h2 class="card-title text-uppercase mb-0">
                      <a href="chi-tiet.html">Lorem ipsum dolor sit</a>
                    </h2>
                  </div>
                </div>
                <div class="col-md-9 d-flex">
                  <a class="thumbnail" href="chi-tiet.html">
                    <div class="thumbnail-inner">
                      <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s1/img-1.jpg" alt="" />
                      <div class="thumbnail-action">
                        <i class="thumbnail-action-icon icont-play-circle"></i>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 d-flex">
            <div class="card card-1">
              <div class="row no-gutters flex-column-reverse flex-md-row">
                <div class="col-md-6">
                  <div class="card-body">
                    <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                    <h2 class="card-title text-uppercase mb-0">
                      <a href="chi-tiet.html">Lorem ipsum dolor sit</a>
                    </h2>
                  </div>
                </div>
                <div class="col-md-6 d-flex">
                  <a class="thumbnail" href="chi-tiet.html">
                    <div class="thumbnail-inner">
                      <img class="thumbnail-img thumbnail-img-size-1" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s1/img-2.jpg" alt="" />
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card card-1 bg-gray">
          <div class="row no-gutters flex-column-reverse flex-md-row">
            <div class="col-md-3">
              <div class="card-body">
                <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                <h2 class="card-title card-title-2 text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed</a>
                </h2>
              </div>
            </div>
            <div class="col-md-9 d-flex">
              <a class="thumbnail" href="chi-tiet.html">
                <div class="thumbnail-inner">
                  <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s2/img-1.jpg" alt="" />
                </div>
              </a>
            </div>
          </div>
        </div>
        <div class="row no-gutters mb-md">
          <div class="col-md-6 d-flex">
            <div class="card card-1">
              <div class="row no-gutters flex-column-reverse flex-md-row">
                <div class="col-md-4">
                  <div class="card-body">
                    <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                    <h2 class="card-title text-uppercase mb-0">
                      <a href="chi-tiet.html">Lorem ipsum dolor sit</a>
                    </h2>
                  </div>
                </div>
                <div class="col-md-8 d-flex">
                  <a class="thumbnail" href="chi-tiet.html">
                    <div class="thumbnail-inner">
                      <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s3/img-1.jpg" alt="" />
                      <div class="thumbnail-action">
                        <i class="thumbnail-action-icon icont-play-circle"></i>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex">
            <div class="card card-1">
              <div class="row no-gutters flex-column-reverse flex-md-row">
                <div class="col-md-4">
                  <div class="card-body">
                    <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                    <h2 class="card-title text-uppercase mb-0">
                      <a href="chi-tiet.html">Lorem ipsum dolor sit</a>
                    </h2>
                  </div>
                </div>
                <div class="col-md-8 d-flex">
                  <a class="thumbnail" href="chi-tiet.html">
                    <div class="thumbnail-inner">
                      <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s3/img-2.jpg" alt="" />
                      <div class="thumbnail-action">
                        <i class="thumbnail-action-icon icont-play-circle"></i>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s4/img-1.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s4/img-2.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s4/img-3.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s4/img-4.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s4/img-5.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s4/img-6.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
        </div>
        <div class="row no-gutters mb-md">
          <div class="col-md-8 d-flex">
            <div class="card card-1">
              <div class="row no-gutters flex-column-reverse flex-md-row">
                <div class="col-md-3">
                  <div class="card-body">
                    <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                    <h2 class="card-title text-uppercase mb-0">
                      <a href="chi-tiet.html">Lorem ipsum dolor sit</a>
                    </h2>
                  </div>
                </div>
                <div class="col-md-9 d-flex">
                  <a class="thumbnail" href="chi-tiet.html">
                    <div class="thumbnail-inner">
                      <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s5/img-1.jpg" alt="" />
                      <div class="thumbnail-action">
                        <i class="thumbnail-action-icon icont-play-circle"></i>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 d-flex">
            <div class="card card-1">
              <div class="row no-gutters flex-column-reverse flex-md-row">
                <div class="col-md-6">
                  <div class="card-body">
                    <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                    <h2 class="card-title text-uppercase mb-0">
                      <a href="chi-tiet.html">Lorem ipsum dolor sit</a>
                    </h2>
                  </div>
                </div>
                <div class="col-md-6 d-flex">
                  <a class="thumbnail" href="chi-tiet.html">
                    <div class="thumbnail-inner">
                      <img class="thumbnail-img thumbnail-img-size-1" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s5/img-2.jpg" alt="" />
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s6/img-1.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s6/img-2.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s6/img-3.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s6/img-4.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s6/img-5.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s6/img-6.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
        </div>
        <div class="card card-1 bg-gray mb-md">
          <div class="row no-gutters flex-column-reverse flex-md-row">
            <div class="col-md-3">
              <div class="card-body">
                <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                <h2 class="card-title card-title-2 text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed</a>
                </h2>
              </div>
            </div>
            <div class="col-md-9 d-flex">
              <a class="thumbnail" href="chi-tiet.html">
                <div class="thumbnail-inner">
                  <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s7/img-1.jpg" alt="" />
                </div>
              </a>
            </div>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s8/img-1.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s8/img-2.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s8/img-3.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s8/img-4.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s8/img-5.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card card-2">
              <div class="card-img">
                <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s8/img-6.jpg" alt="" />
              </div>
              <div class="card-img-overlay">
                <p class="card-meta text-uppercase mb-1">Lorem ipsum dolor sit amet</p>
                <h2 class="card-title text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</a>
                </h2>
              </div>
            </div>
          </div>
        </div>
        <div class="row no-gutters mb-md">
          <div class="col-md-8 d-flex">
            <div class="card card-1">
              <div class="row no-gutters flex-column-reverse flex-md-row">
                <div class="col-md-3">
                  <div class="card-body">
                    <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                    <h2 class="card-title text-uppercase mb-0">
                      <a href="chi-tiet.html">Lorem ipsum dolor sit</a>
                    </h2>
                  </div>
                </div>
                <div class="col-md-9 d-flex">
                  <a class="thumbnail" href="chi-tiet.html">
                    <div class="thumbnail-inner">
                      <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s9/img-1.jpg" alt="" />
                      <div class="thumbnail-action">
                        <i class="thumbnail-action-icon icont-play-circle"></i>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 d-flex">
            <div class="card card-1">
              <div class="row no-gutters flex-column-reverse flex-md-row">
                <div class="col-md-6">
                  <div class="card-body">
                    <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                    <h2 class="card-title text-uppercase mb-0">
                      <a href="chi-tiet.html">Lorem ipsum dolor sit</a>
                    </h2>
                  </div>
                </div>
                <div class="col-md-6 d-flex">
                  <a class="thumbnail" href="chi-tiet.html">
                    <div class="thumbnail-inner">
                      <img class="thumbnail-img thumbnail-img-size-1" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s9/img-2.jpg" alt="" />
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="card card-1 bg-gray">
          <div class="row no-gutters flex-column-reverse flex-md-row">
            <div class="col-md-3">
              <div class="card-body">
                <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                <h2 class="card-title card-title-2 text-uppercase mb-0">
                  <a href="chi-tiet.html">Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed</a>
                </h2>
              </div>
            </div>
            <div class="col-md-9 d-flex">
              <a class="thumbnail" href="chi-tiet.html">
                <div class="thumbnail-inner">
                  <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s10/img-1.jpg" alt="" />
                </div>
              </a>
            </div>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-md-6 d-flex">
            <div class="card card-1">
              <div class="row no-gutters flex-column-reverse flex-md-row">
                <div class="col-md-4">
                  <div class="card-body">
                    <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                    <h2 class="card-title text-uppercase mb-0">
                      <a href="chi-tiet.html">Lorem ipsum dolor sit</a>
                    </h2>
                  </div>
                </div>
                <div class="col-md-8 d-flex">
                  <a class="thumbnail" href="chi-tiet.html">
                    <div class="thumbnail-inner">
                      <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s11/img-1.jpg" alt="" />
                      <div class="thumbnail-action">
                        <i class="thumbnail-action-icon icont-play-circle"></i>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex">
            <div class="card card-1">
              <div class="row no-gutters flex-column-reverse flex-md-row">
                <div class="col-md-4">
                  <div class="card-body">
                    <p class="card-meta small text-muted mb-1">Lorem ipsum</p>
                    <h2 class="card-title text-uppercase mb-0">
                      <a href="chi-tiet.html">Lorem ipsum dolor sit</a>
                    </h2>
                  </div>
                </div>
                <div class="col-md-8 d-flex">
                  <a class="thumbnail" href="chi-tiet.html">
                    <div class="thumbnail-inner">
                      <img class="thumbnail-img" src="<?= get_template_directory_uri() ?>/assets/images/ex/home/s11/img-2.jpg" alt="" />
                      <div class="thumbnail-action">
                        <i class="thumbnail-action-icon icont-play-circle"></i>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
		<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template() {
		?>
		<#
		view.addInlineEditingAttributes( 'title', 'none' );
		view.addInlineEditingAttributes( 'description', 'basic' );
		view.addInlineEditingAttributes( 'content', 'advanced' );
		#>
		<h2 {{{ view.getRenderAttributeString( 'title' ) }}}>{{{ settings.title }}}</h2>
		<div {{{ view.getRenderAttributeString( 'description' ) }}}>{{{ settings.description }}}</div>
		<div {{{ view.getRenderAttributeString( 'content' ) }}}>{{{ settings.content }}}</div>
		<?php
	}
}
